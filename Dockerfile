FROM php:8.3

RUN apt update \
    && apt install --no-install-recommends -y \
        wget lsb-release curl gpg ca-certificates zip git \
    && apt-get clean

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions zip gd bcmath intl redis soap

RUN curl -sS http://getcomposer.org/installer | php -- --filename=composer \
    && chmod a+x composer \
    && mv composer /usr/local/bin/composer

RUN mkdir -p /data/htdocs && chown -R www-data:www-data /data

WORKDIR /data/htdocs